/* LICENSE START */
/*
 * ssoup
 * https://github.com/alebellu/ssoup
 *
 * Copyright 2012 Alessandro Bellucci
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://alebellu.github.com/licenses/GPL-LICENSE.txt
 * http://alebellu.github.com/licenses/MIT-LICENSE.txt
 */
/* LICENSE END */

/* HEADER START */
var env, requirejs, define;
if (typeof module !== 'undefined' && module.exports) {
    env = "node";
    requirejs = require('requirejs');
    define = requirejs.define;

    // require all dependant libraries, so they will be available for the client to request.
    var pjson = require('./package.json');
    for (var library in pjson.dependencies) {
        require(library);
    }
} else {
    env = "browser";
    //requirejs = require;
}

({ define: env === "browser"
        ? define
        : function(A,F) { // nodejs.
            var path = require('path');
            var moduleName = path.basename(__filename, '.js');
            console.log("Loading module " + moduleName);
            module.exports = F.apply(null, A.map(require));
            global.registerUrlContext("/" + moduleName + ".js", {
                "static": __dirname,
                defaultPage: moduleName + ".js"
            });
            global.registerUrlContext("/" + moduleName, {
                "static": __dirname
            });
        }
}).
/* HEADER END */
define(['jquery', 'fs', 'artusi-kitchen-tools'], function($, fs, tools) {

    var drdf = tools.drdf;
    var drff = tools.drff;

    var filesystemDrawerType = function(context) {
        var drawer = this;
        drawer.context = context;
    };

    /**
     * @options:
     *   @drawerIRI the IRI of the drawer.
     */
    filesystemDrawerType.prototype.init = function() {
        var dr = $.Deferred();
        var drawer = this;
        var kitchen = drawer.context.kitchen;

        // copy filesystem specific options into easy typing options.
        drawer.conf.path = drawer.conf["afs:path"];

        /**
         * Registry indexes.
         */
        drawer.indexes = {};

        /**
         * Code of the recipes.
         */
        drawer.recipesCode = {};

        drawer.recipeBookCache = {};

        drawer.knows({
            resourceIRI: 'context.jsonld'
        }).done(function(known){
            if (known) {
                drawer.getResource({
                    resourceIRI: 'context.jsonld'
                }).done(function(context) {
                    drawer.context = context;
                    dr.resolve();
                }).fail(drff(dr));
            } else {
                drawer.knows({
                    resourceIRI: 'context.ttl'
                }).done(function(known){
                    if (known) {
                        drawer.getResource({
                            resourceIRI: 'context.ttl'
                        }).done(function(ttlContext) {
                            kitchen.tools.parseTurtleContext(ttlContext).done(function(context) {
                                drawer.context = context;
                                dr.resolve();
                            }).fail(drff(dr));
                        }).fail(drff(dr));
                    } else {
                        dr.resolve();
                    }
                }).fail(drff(dr));
            }
        }).fail(drff(dr));

        return dr.promise();
    };

    filesystemDrawerType.prototype.getIcon32Url = function() {
        return "http://www.mongodb.org/download/attachments/132305/PoweredMongoDBbeige50.png?version=1&modificationDate=1247081511792";
    };

    filesystemDrawerType.prototype.getServiceRecipes = function(options) {
        var drawer = this;
        var dr = $.Deferred();

        dr.resolve([]);

        return dr.promise();
    };

    filesystemDrawerType.prototype.ssoupRecipe = function(recipeIRI, recipeCode) {
        this.recipesCode[recipeIRI] = recipeCode;
    };

    /**
     * Artusi implementation of SSOUP [drawer.knows](https://github.com/alebellu/ssoup/blob/master/concepts.md#drawer.knows)
     *
     * @options:
     * 	@resourceIRI the IRI of the resource to check the existence
     * @return true if the registry contains the resource with the given IRI as a subject of at least one triple, false otherwise.
     */
    filesystemDrawerType.prototype.knows = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        var resourceIRI = drawer.getRelativeResourceIRI(options.resourceIRI);
        if (resourceIRI) {
            var path = drawer.conf.path + resourceIRI;

            fs.exists(path, function(exists) {
                dr.resolve(exists)
            });
        } else {
            dr.resolve(false);
        }

        return dr.promise();
    };

    /**
     * Artusi implementation of SSOUP [drawer.get](https://github.com/alebellu/ssoup/blob/master/concepts.md#drawer.get)
     *
     * @options:
     *   @resourceIRI the IRI of the resource to retrieve
     *   @postProcess how to post process the resource: none, eval, jQuery; defaults to eval
     * @return the resource.
     */
    filesystemDrawerType.prototype.getResource = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        console.info("Retrieving " + options.resourceIRI);
        var resourceIRI = drawer.getRelativeResourceIRI(options.resourceIRI);
        if (resourceIRI) {
            var path = drawer.conf.path + resourceIRI;

            // Make sure the file is okay
            fs.stat(path, function(err, stats) {
                if(err) {
                    if (err.code == "ENOENT") {
                        // file not found
                        dr.resolve(undefined);
                    } else {
                        dr.reject(err);
                    }
                    return;
                }

                if (stats.isFile()) {
                    fs.readFile(path, 'utf8', function (err, data) {
                        if(err) { dr.reject(err); return; }
        
                        dr.resolve(data);
                    });
                } else {
                    dr.resolve(undefined);
                }
            });
        } else {
            dr.resolve(undefined);
        }

        return dr.promise();
    };

    /**
     * Artusi implementation of SSOUP [drawer.put](https://github.com/alebellu/ssoup/blob/master/concepts.md#drawer.put)
     *
     * @options:
     *    @resourceIRI the IRI of the resource to save
     *    @data the data to save to the register
     */
    filesystemDrawerType.prototype.put = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        // resourceIRI can be either absolute or relative to the drawer path
        /*var resourceIRI = drawer.getRelativeResourceIRI(options.resourceIRI);
        var path = drawer.conf.path + '/' + resourceIRI;

        var resourceIRI = drawer.applyContext(options.resourceIRI);
        drawer.db.collection(resourceIRI, function(err, collection) {
            if(err) { dr.reject(err); return; }

            console.info("Updating collection " + options.resourceIRI + ", document @id=" + options.data["@id"]);
            collection.update({}, options.data, {w: 1}, function(err, nrows) {
                console.info("Collection " + options.resourceIRI + ", document @id=" + options.data["@id"] + " updated. (" + nrows + " rows).");
                dr.resolve();
            });
        });*/
        dr.resolve();

        return dr.promise();
    };

    /**
     * Retrieves info about the indexes in the drawer.
     *
     * @options
     */
    filesystemDrawerType.prototype.getIndexes = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        dr.resolve();

        return dr.promise();
    };

    /**
     * Build all the indexes by scanning all the resources in the drawer.
     *
     * @options
     */
    filesystemDrawerType.prototype.buildIndexes = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        dr.resolve();

        return dr.promise();
    }

    /**
     * Build an index by scanning all the resources in the drawer.
     *
     * @options
     *	@indexInfo the info of the index to rebuild.
     */
    filesystemDrawerType.prototype.buildIndex = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        dr.resolve();

        return dr.promise();
    };

    /**
     * Update an index by rescanning all the resources in the specified tree.
     * Blob fetching can be parallelized.
     *
     * @options
     *	@indexInfo the info structure about the index to build
     *	@index the index to update.
     *	@tree the tree to scan.
     * @return an indexing structure for the tree
     */
    filesystemDrawerType.prototype.buildIndexForTree = function(options) {
        var deferreds = [];
        var drawer = this;

        dr.resolve();

        return dr.promise();
    };

    return filesystemDrawerType;
});
